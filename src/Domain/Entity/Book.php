<?php
declare(strict_types=1);

namespace App\Domain\Entity;

use Assert\Assertion;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $author;

    /**
     * @ORM\Column(type="integer")
     */
    private int $priceInCents;

    public function __construct(string $title, string $author, int $priceInCents)
    {
        Assertion::notEmpty($title, null, 'title');
        Assertion::notEmpty($author, null, 'author');
        Assertion::greaterThan($priceInCents, 0, null, 'priceInCents');

        $this->title = $title;
        $this->author = $author;
        $this->priceInCents = $priceInCents;
    }

    public function __toString(): string
    {
        return $this->getTitle().' | '.$this->getAuthor();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getPriceInCents(): int
    {
        return $this->priceInCents;
    }
}
