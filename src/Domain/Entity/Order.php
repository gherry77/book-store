<?php
declare(strict_types=1);

namespace App\Domain\Entity;

use App\Domain\Exceptions\CannotEditPaidOrderException;
use App\Domain\Exceptions\InvalidDiscountPercentageException;
use App\Domain\Exceptions\InvalidEmailException;
use App\Domain\Exceptions\InvalidFullNameException;
use App\Domain\Exceptions\OrderAlreadyPaidException;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="order_table")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $fullName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $email;

    /**
     * @ORM\Column(type="integer")
     */
    private int $discountPercentage;

    /**
     * @ORM\ManyToOne(targetEntity=App\Domain\Entity\Book::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Book $book;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private int $amountInCents;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private bool $paid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $paymentId;

    /**
     * @throws AssertionFailedException
     * @throws InvalidDiscountPercentageException
     * @throws InvalidEmailException
     * @throws InvalidFullNameException
     */
    public function __construct(string $fullName, string $email, Book $book, int $discountPercentage)
    {
        Assertion::notEmpty($fullName);
        Assertion::email($email);
        Assertion::range($discountPercentage, 0, 50);

        if (empty($fullName)) {
            throw new InvalidFullNameException();
        }

        if (!filter_var($email, \FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailException();
        }

        if ($discountPercentage > 50 || $discountPercentage < 0) {
            throw new InvalidDiscountPercentageException();
        }

        $this->fullName = $fullName;
        $this->email = $email;
        $this->book = $book;
        $this->discountPercentage = $discountPercentage;
        $this->paid = false;
        $this->paymentId = null;

        $this->amountInCents = (int) ($book->getPriceInCents() * (100 - $discountPercentage) / 100);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getDiscountPercentage(): int
    {
        return $this->discountPercentage;
    }

    public function getBook(): Book
    {
        return $this->book;
    }

    public function getAmountInCents(): int
    {
        return $this->amountInCents;
    }

    public function getPaid(): bool
    {
        return $this->paid;
    }

    public function getPaymentId(): ?string
    {
        return $this->paymentId;
    }

    /**
     * @throws AssertionFailedException
     * @throws CannotEditPaidOrderException
     */
    public function changeEmail(string $email): void
    {
        Assertion::email($email);

        if ($this->paid) {
            throw new CannotEditPaidOrderException();
        }

        $this->email = $email;
    }

    /**
     * @throws AssertionFailedException
     * @throws OrderAlreadyPaidException
     */
    public function addPayment(string $paymentId): void
    {
        Assertion::notEmpty($paymentId);

        if ($this->paid) {
            throw new OrderAlreadyPaidException();
        }

        $this->paid = true;
        $this->paymentId = $paymentId;
    }

    // Nessun altro metodo oltre ai getter
}
