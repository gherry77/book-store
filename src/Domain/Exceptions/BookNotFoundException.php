<?php

declare(strict_types=1);

namespace App\Domain\Exceptions;

use Exception;

class BookNotFoundException extends Exception
{
    private int $bookId;

    public function __construct(int $bookId)
    {
        parent::__construct('exception.book_not_found');
        $this->bookId = $bookId;
    }

    public function getBookId(): int
    {
        return $this->bookId;
    }
}
