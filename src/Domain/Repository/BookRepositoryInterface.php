<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\Book;
use App\Domain\Exceptions\BookNotFoundException;

interface BookRepositoryInterface
{
    public function save(Book $book): void;

    /**
     * @throws BookNotFoundException
     */
    public function findById(int $id): Book;

    /**
     * @return Book[]
     */
    public function findByTitle(string $title): array;
}
