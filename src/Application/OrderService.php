<?php
declare(strict_types=1);

namespace App\Application;

use App\Application\Dto\CreateOrderDto;
use App\Domain\Entity\Order;
use App\Repository\BookRepository;
use App\Repository\OrderRepository;

class OrderService
{
    private OrderRepository $orderRepository;
    private BookRepository $bookRepository;

    public function __construct(OrderRepository $orderRepository, BookRepository $bookRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->bookRepository = $bookRepository;
    }

    public function create(CreateOrderDto $createOrderDto): int
    {
        // throws BookNotFoundException
        $book = $this->bookRepository->findOneById($createOrderDto->getBookId());

        $order = new Order(
            $createOrderDto->getFullName(),
            $createOrderDto->getEmail(),
            $book,
            $createOrderDto->getDiscountPercentage(),
        );

        $this->orderRepository->save($order);

        return $order->getId();
    }
}
