<?php
declare(strict_types=1);

namespace App\Application\Dto;

use App\Domain\Entity\Book;
use Symfony\Component\Validator\Constraints as Assert;

class CreateOrderDto
{
    /**
     * @Assert\NotBlank
     */
    private string $fullName;

    /**
     * @Assert\Email
     * @Assert\NotBlank
     */
    private string $email;

    /**
     * @Assert\NotBlank
     */
    private int $bookId;

    /**
     * @Assert\NotBlank
     * @Assert\Range(min=0, max=50)
     */
    private int $discountPercentage;

    /**
     * Get the value of fullName.
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * Set the value of fullName.
     *
     * @param mixed $fullName
     *
     * @return self
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get the value of email.
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Set the value of email.
     *
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of bookId.
     */
    public function getBookId(): int
    {
        return $this->bookId;
    }

    /**
     * Set the value of bookId.
     *
     * @param mixed $bookId
     *
     * @return self
     */
    public function setBookId($bookId)
    {
        if ($bookId instanceof Book) {
            $bookId = $bookId->getId();
        }
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get the value of discountPercentage.
     */
    public function getDiscountPercentage(): ?int
    {
        return $this->discountPercentage;
    }

    /**
     * Set the value of discountPercentage.
     *
     * @param mixed $discountPercentage
     *
     * @return self
     */
    public function setDiscountPercentage($discountPercentage)
    {
        $this->discountPercentage = $discountPercentage;

        return $this;
    }
}
