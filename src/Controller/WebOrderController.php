<?php
declare(strict_types=1);

namespace App\Controller;

use App\Application\Dto\CreateOrderDto;
use App\Application\OrderService;
use App\Domain\Entity\Order;
use App\Form\CreateOrderType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class WebOrderController extends AbstractController
{
    #[Route('/create-order', name: 'create_order')]
    public function index(OrderService $orderService, Request $request): Response
    {
        $form = $this->createForm(CreateOrderType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $normalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());

            try {
                // $createOrderDto = $normalizer->denormalize(
                //     $form->getData(),
                //     CreateOrderDto::class
                // );

                // var_dump(get_class($form->getData()));
                // var_dump($form->getData());
                // var_dump($createOrderDto);
                // die('wqwqwqw');

                $createOrderDto = $form->getData();

                $id = $orderService->create($createOrderDto);
                $this->addFlash('success', sprintf('Order #%s created sucessfully', $id));
            } catch (Exception $e) {
                //throw $e;
                $form->addError(new FormError($e->getMessage()));
            }
        }

        return $this->render('create-order.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    // #[Route('/pay-order', name: 'pay_order')]
    // public function payOrder(Request $request): Response
    // {
    //     //get $order fropm repository...
    //     $order->setPaid(true);
    //     $this->em->flush();
    // }

    // #[Route('/import', name: 'import')]
    // public function import(Request $request, EntityManagerInterface $em): Response
    // {
    //     $order = new Order();
    //     $order->setEmail('invalid-email');
    //     $order->setDiscountPercentage(80);
    //     $order->setAmountInCents(-100);

    //     $em->persist($order);
    //     $em->flush();
    // }
}
