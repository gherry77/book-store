<?php
declare(strict_types=1);

namespace App\Controller;

use App\Application\Dto\CreateOrderDto;
use App\Application\OrderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiOrderController extends AbstractController
{
    private OrderService $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    #[Route('/api/create-order', name: 'api_create_order')]
    public function createOrder(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $constraints = new Collection([
            'fullName' => new NotBlank(),
            'email' => [new NotBlank(), new Email()],
            'discountPercentage' => [new NotBlank(), new Range(['min' => 0, 'max' => 50])],
            'bookId' => [new NotBlank(), new Type('int')],
        ]);

        $violations = $validator->validate($request->toArray(), $constraints);

        if (\count($violations) > 0) {
            return new JsonResponse(['validationErrors' => $this->formatErrors($violations)], 400);
        }

        $normalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
        $createOrderDto = $normalizer->denormalize(
            $request->toArray(),
            CreateOrderDto::class
        );
        $orderId = $this->orderService->create($createOrderDto);

        return new JsonResponse(['order_id' => $orderId]);
    }

    /**
     * @param \Traversable<ConstraintViolationInterface> $violations
     *
     * @return array<String>
     */
    private function formatErrors($violations)
    {
        $messages = [];

        foreach ($violations as $violation) {
            $paramName = $violation->getPropertyPath();
            $messages[$paramName] = $violation->getMessage();
        }

        return $messages;
    }
}
