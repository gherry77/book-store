<?php

declare(strict_types=1);

namespace App\Form;

use App\Application\Dto\CreateOrderDto;
use App\Domain\Entity\Book;
use App\Repository\BookRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateOrderType extends AbstractType
{
    private BookRepository $bookRepository;

    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fullName', TextType::class)
            ->add('email', EmailType::class)
            ->add('discountPercentage', IntegerType::class)
            ->add('bookId', ChoiceType::class, [
                'choices' => $this->getBookChoices(),
            ])
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CreateOrderDto::class,
        ]);
    }

    /**
     * @return int[]
     */
    private function getBookChoices(): array
    {
        /** @var Book[] $books */
        $books = $this->bookRepository->findAll();
        $choices = [];
        foreach ($books as $book) {
            $label = $book->getTitle().' | '.$book->getAuthor();
            $choices[$label] = $book->getId();
        }

        return $choices;
    }
}
