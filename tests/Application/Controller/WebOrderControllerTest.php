<?php
declare(strict_types=1);

namespace App\Tests\Application\Controller;

use App\Domain\Entity\Book;
use App\Domain\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WebOrderControllerTest extends WebTestCase
{
    protected KernelBrowser $client;

    protected EntityManagerInterface $em;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = static::createClient();
        $this->em = $this->getContainer()->get(EntityManagerInterface::class);
        $this->givenOrdersNoData();
        $this->givenBooksNoData();
    }

    public function test_get_create_order(): void
    {
        $client = $this->getClient();
        $crawler = $client->request('GET', '/create-order');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Book Store');
    }

    public function test_post_create_order_message_error(): void
    {
        $client = $this->getClient();
        $crawler = $client->request('GET', '/create-order');

        // // select the button
        // $buttonCrawlerNode = $crawler->selectButton('create_order[save]');

        // // retrieve the Form object for the form belonging to this button
        // $form = $buttonCrawlerNode->form();

        $client->submitForm('Save', [
            'create_order[fullName]' => null,
            'create_order[email]' => 'aaa',
            'create_order[discountPercentage]' => 80,
            //'create_order[bookId]' => '1',
            //'create_order[save]' => 'save',
        ]);

        $this->assertResponseIsSuccessful();

        $content = $client->getResponse()->getContent();
        //var_dump(get_class_methods($this));
        $this->assertStringContainsString('This value should not be blank', $content);
    }

    public function test_post_create_order_ok(): void
    {
        //---Given
        $book = new Book('Titolo', 'Autore', 500);

        $this->em->persist($book);
        $this->em->flush();

        $order = new Order('Pinco Pallino', 'test@test.it', $book, 10);

        //---When
        $client = $this->getClient();
        $client->request('GET', '/create-order');

        $client->submitForm('Save', [
            'create_order[fullName]' => $order->getFullName(),
            'create_order[email]' => $order->getEmail(),
            'create_order[discountPercentage]' => $order->getDiscountPercentage(),
            'create_order[bookId]' => $book->getId(),
            //'create_order[save]' => 'save',
        ]);

        //---Then
        $this->assertResponseIsSuccessful();

        $content = $client->getResponse()->getContent();
        //var_dump(get_class_methods($this));
        $this->assertStringNotContainsString('This value should not be blank', $content);
    }

    protected function getClient(): KernelBrowser
    {
        return $this->client;
    }

    protected function givenBooksNoData(): void
    {
        $this->em->createQuery('delete from App\Domain\Entity\Book')->execute();
    }

    protected function givenOrdersNoData(): void
    {
        $this->em->createQuery('delete from App\Domain\Entity\Order')->execute();
    }
}
