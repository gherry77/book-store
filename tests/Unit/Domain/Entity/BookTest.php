<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Entity;

use App\Domain\Entity\Book;
use Assert\AssertionFailedException;
use PHPUnit\Framework\TestCase;

class BookTest extends TestCase
{
    /**
     * @dataProvider data_test_book_throw_exception
     */
    public function test_book_throw_exception(string $title, string $author, int $priceInCents): void
    {
        $this->expectException(AssertionFailedException::class);

        new Book($title, $author, $priceInCents);
    }

    /**
     * @return array[]
     */
    public function data_test_book_throw_exception(): array
    {
        return [
            ['', uniqid(), 0],
            [uniqid(), '', 0],
            [uniqid(), uniqid(), -1],
        ];
    }

    public function test_book_ok(): void
    {
        $title = uniqid();
        $author = uniqid();
        $priceInCents = 1000;

        $book = new Book($title, $author, $priceInCents);

        $this->assertEquals($title, $book->getTitle());
        $this->assertEquals($author, $book->getAuthor());
        $this->assertEquals($priceInCents, $book->getPriceInCents());
        $this->assertEquals($title.' | '.$author, (string) $book);
    }
}
