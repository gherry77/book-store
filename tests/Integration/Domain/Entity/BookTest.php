<?php
declare(strict_types=1);

namespace App\Tests\Integration\Domain\Entity;

use App\Domain\Entity\Book;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BookTest extends KernelTestCase
{
    protected EntityManagerInterface $em;

    protected function setUp(): void
    {
        parent::setUp();

        $this->em = $this->getContainer()->get(EntityManagerInterface::class);
        $this->givenOrdersNoData();
        $this->givenBooksNoData();
    }

    public function test_book_get_id(): void
    {
        $book = new Book('Titolo', 'Autore', 500);

        $this->em->persist($book);
        $this->em->flush();

        $this->assertGreaterThan(0, $book->getId());
    }

    protected function givenBooksNoData(): void
    {
        $this->em->createQuery('delete from App\Domain\Entity\Book')->execute();
    }

    protected function givenOrdersNoData(): void
    {
        $this->em->createQuery('delete from App\Domain\Entity\Order')->execute();
    }
}
